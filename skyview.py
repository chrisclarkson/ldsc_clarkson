#!/usr/bin/env python
from skyline.example_data import examples
from matplotlib import gridspec
from skyline import (
    figure as skyfig,
    scatter,
    coords,
    utils,
    labels
)
import os
import pprint as pp
from skyline.utils import downscale
import pandas as pd
import numpy as np
import argparse

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

parser = argparse.ArgumentParser(
    description="input params to plot skyview"
)
parser.add_argument('--list_of_files',
    type=str,
    nargs='+',
    help="list sumstat file",
    default=None
)
parser.add_argument('--label_with_file_names',
    type=str2bool,
    help='label the file names',
    default=False
)
parser.add_argument('--list_of_patterns_omit',
    type=str,
    nargs='+',
    help="list of patterns to omit from label names",
    default=None
)
parser.add_argument('--file',
    type=str,
    help="file with list of sumstat files",
    default=None
)
parser.add_argument('--input_dir',
    type=str,
    help="dir containing sumstat files",
    default='.'
)
parser.add_argument('--chr_name',
    type=str,
    help='chromosome column',
    default='chr_name'
)
parser.add_argument('--start_pos',
    type=str,
    help='start pos column',
    default='start_pos'
)
parser.add_argument('--pvalue',
    type=str,
    help='pvalue column',
    default='pvalue'
)
parser.add_argument('--limits',
    type=float,
    nargs='+',
    help="list of thresholds for color",
    default=None
)
parser.add_argument('--colors',
    type=str,
    nargs='+',
    help="list of colors for each limit",
    default=None
)
parser.add_argument('--output',
    type=str,
    help='output',
    default='skyview_scatter.png'
)
parser.add_argument('--output_dir',
    type=str,
    help='output dir',
    default='.'
)
parser.add_argument('--log10',
    type=str2bool,
    help='convert pvalues to -log10 values',
    default=False
)
args = parser.parse_args()

try:
    int(args.chr_name)
    args.chr_name=int(args.chr_name)
except:
    pass

try:
    int(args.start_pos)
    args.start_pos=int(args.start_pos)
except:
    pass

try:
    int(args.pvalue)
    args.pvalue=int(args.pvalue)
except:
    pass


def format_file(file,chr_name='chr_name',start_pos='start_pos', pvalue='pvalue',log10=False):
    test_gwas = pd.read_csv(file,sep='\t',compression='gzip')
    test_gwas=test_gwas[[chr_name, start_pos, pvalue]]
    test_gwas.columns=['chr_name', 'start_pos', 'pvalue']
    if log10:
        test_gwas['pvalue']=-np.log10(test_gwas['pvalue'])
    test_gwas=downscale(test_gwas) #,start_pos=8.5e05
    test_gwas['chr_name']=test_gwas['chr_name'].astype(str)
    b37_coords = coords.HumanGRCh37(default_pad=default_pad)
    chrs=test_gwas['chr_name'].unique().tolist()
    b37_coords.set_inactive(chrs)
    return test_gwas,b37_coords

# limits = [(15.301, '#FEAE24'), (10.301, '#F04424'), (7.301, '#E14228')]

if args.limits is None:
    limits=[15.03,10.3,7.3]
else:
    limits=args.limits

if args.colors is None:
    colors=['#FEAE24','#F04424','#E14228']
else:
    colors=args.colors

lims=[]
for l,c in zip(limits,colors):
    lims.append((l,c))

limits=lims



def scatter_plot(gfig,test_gwas,genomic_tracks,i,b37_coords,label=None):
    marker_styles = [
        utils.cmap_style_update(
            i, dict(marker=marker, c=col1, s=size, edgecolors=None), limits=limits
        ),
        utils.cmap_style_update(
            i, dict(marker=marker, c=col2, s=size, edgecolors=None), limits=limits
        )
    ]
    if label is not None:
        scatter_track = scatter.SkyViewPlot(
            gfig,
            genomic_tracks[i:i+1],
            test_gwas,
            b37_coords,
            marker_styles=marker_styles,
            ylabel=label
        )
    else:
        scatter_track = scatter.SkyViewPlot(
            gfig,
            genomic_tracks[i:i+1],
            test_gwas,
            b37_coords,
            marker_styles=marker_styles
        )
    gfig.add_track(scatter_track)
    return gfig

if args.list_of_files is not None:
    files=args.list_of_files
elif args.file is not None:
    files=[]
    with open(args.file) as f:
        lines=f.readlines()
        for line in lines:
            line=line.rstrip()
            files.append(line)

dpi = 100
nrows = len(files)+1
ncols = 1
size = 1
default_pad = 1E6
marker = "o"
col1 = '#6495ED03'
col2 = '#00006C03'

genomic_tracks = gridspec.GridSpec(
    nrows, ncols, wspace=0, hspace=0.3
)
multiplier = 1.5
figsize = [7.4 * multiplier, 3.8 * multiplier]
gfig = skyfig.GenomicFigure(figsize=figsize, dpi=dpi)

for i in range(len(files)):
    file=files[i]
    print(file)
    if args.label_with_file_names:
        label=file
        for pat in args.list_of_patterns_omit:
            label=label.replace(pat,'')
    else:
        label=None
    file=os.path.join(args.input_dir,file)
    file,b37_coords=format_file(file,args.chr_name,args.start_pos,args.pvalue,args.log10)
    gfig=scatter_plot(gfig,file,genomic_tracks,i,b37_coords,label)

fro=nrows-2
to=nrows-1
chr_labels = labels.ChrAxis(
    gfig,
    genomic_tracks[fro:to],
    b37_coords,
    rows=2,
    offset=-2.5
)
gfig.add_track(chr_labels)
gfig.savefig(os.path.join(args.output_dir, args.output), bbox_inches='tight')


